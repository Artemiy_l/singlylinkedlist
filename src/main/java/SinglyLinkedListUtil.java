public class SinglyLinkedListUtil {

    public static SinglyLinkedList[] splitOddsEvensReversed(SinglyLinkedList.Node head) {
        SinglyLinkedList input = new SinglyLinkedList(head);
        SinglyLinkedList odds = input.filter(i -> i % 2 != 0);
        SinglyLinkedList[] result = new SinglyLinkedList[2];
        result[0] = odds;
        SinglyLinkedList evens = input.filter(i -> i % 2 == 0);
        result[1] = evens;
        return result;
    }
}
