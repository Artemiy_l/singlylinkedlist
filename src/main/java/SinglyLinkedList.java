import java.util.Objects;
import java.util.function.Predicate;

public class SinglyLinkedList {

    private int size;
    private Node head;

    public SinglyLinkedList() {}

    public SinglyLinkedList(SinglyLinkedList.Node head) {
        this.head = head;
    }

    public Node getHead() {
        return head;
    }

    public int size() {
        return size;
    }

    public void add(Integer i) {
        head = new Node(i, head);
        size++;
    }

    SinglyLinkedList filter(Predicate<Integer> filter) {
        SinglyLinkedList result = new SinglyLinkedList();
        Node node = head;
        while (node != null) {
            if (filter.test(node.item)) {
                result.add(node.item);
            }
            node = node.previous;
        }
        return result;
    }

    public void reverse() {
        Node node = head;
        while (node.previous != null) {
            Node temp = node.previous;
            node.previous = temp.previous;
            temp.previous = head;
            head = temp;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SinglyLinkedList)) return false;
        SinglyLinkedList other = (SinglyLinkedList) obj;
        if (size != other.size) return false;
        Node node = head;
        Node otherNode = other.head;
        while (node != null && otherNode != null) {
            if (!Objects.equals(node.item, otherNode.item)) return false;
            node = node.previous;
            otherNode = otherNode.previous;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node node = head;
        while (node != null) {
            builder.append(node.item)
                    .append(' ');
            node = node.previous;
        }
        return builder.reverse()
                .toString();
    }

    static class Node {
        Integer item;
        Node previous;

        Node(Integer element, Node previous) {
            this.item = element;
            this.previous = previous;
        }
    }
}
