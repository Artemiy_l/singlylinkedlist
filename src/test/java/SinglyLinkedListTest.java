import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SinglyLinkedListTest {

    SinglyLinkedList list;

    @Before
    public void initList() {
        list = new SinglyLinkedList();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
    }

    @Test
    public void testFilterOdds() {
        SinglyLinkedList filtered = list.filter(i -> i % 2 != 0);
        assertEquals(2, filtered.size());
    }

    @Test
    public void testFilterEven() {
        SinglyLinkedList filtered = list.filter(i -> i % 2 == 0);
        assertEquals(3, filtered.size());
    }

    @Test
    public void testReverse() {
        String expected = new StringBuilder(list.toString())
                .reverse()
                .toString()
                .trim();

        list.reverse();
        String actual = list.toString()
                .trim();

        assertEquals(expected, actual);
    }
}