import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SinglyLinkedListUtilTest {

    private SinglyLinkedList list;

    @Before
    public void initList() {
        list = new SinglyLinkedList();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
    }

    @Test
    public void testSplitOddsEvensReversed() {
        SinglyLinkedList[] actual = SinglyLinkedListUtil.splitOddsEvensReversed(list.getHead());
        SinglyLinkedList odds = list.filter(i -> i % 2 != 0);
        SinglyLinkedList evens = list.filter(i -> i % 2 == 0);
        assertArrayEquals(new SinglyLinkedList[]{odds, evens}, actual);
    }
}